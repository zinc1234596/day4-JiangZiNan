package com.afs.tdd;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command command) {
        switch (command) {
            case M:
                move();
                break;
            case L:
                turnLeft();
                break;
            case R:
                turnRight();
                break;
        }
        return location;
    }

    public Location executeCommands(String commands) {
        for (char c : commands.toCharArray()) {
            Command command = Command.valueOf(String.valueOf(c));
            executeCommand(command);
        }
        return location;
    }


    private void move() {
        switch (location.getDirection()) {
            case North:
                location.setCoordinateY(location.getCoordinateY() + 1);
                break;
            case South:
                location.setCoordinateY(location.getCoordinateY() - 1);
                break;
            case West:
                location.setCoordinateX(location.getCoordinateX() - 1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX() + 1);
                break;
        }
    }

    private void turnLeft() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
        }
    }

    private void turnRight() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
    }
}

# ORID Daily Report (2023/07/12)

## O

This morning, I reviewed the knowledge of the Observer pattern and started learning TDD.

## R

At first, I found TDD to be cumbersome and tedious since I had never encountered it before. I didn't see much value in it. As for the Observer pattern, although I learned it in college, I had almost forgotten it without reviewing.

## I

After learning and practicing TDD, I realized its importance. TDD can improve code quality, enhance maintainability, and speed up development. It also provides safety. As for the Observer pattern, it reduces the coupling between objects and proves to be a useful design pattern.

## D

In the future, I will periodically review previously learned knowledge to prevent forgetting. I should also practice and digest newly acquired knowledge in a timely manner. Reflecting on the reasons behind its creation will help me better understand it.